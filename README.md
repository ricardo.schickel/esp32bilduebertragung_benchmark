# ESP32Bildübertragung_Benchmark



## Zweck 

In diesem Projekt werden zwei Mikrocontroller-basierte Systeme (ESP32) für die Bildübertragung über eine kabelgebundene Busübertragung getestet. Hierbei kommen die Busübertragungssysteme I2C und SPI zum Einsatz, welche untereinander durch Funktionstests verglichen werden können. Es werden die Parameter FPS (Frames per Second), Übertragungsgeschwindigkeit, Prozentuale Abweichung vom Mittelwert der Pixelwerte, und Latenzzeiten zur Leistungsbewertung ausgegeben.

Der Code kann leicht angepasst werden, um die Leistung in verschiedenen Szenarien, Bildauflösungen und Frequenzen zu untersuchen.

## Hardware-Komponenten

Master: Waveshare ESP32 one <br>
Slave: ESP32-DevKitC V4

## Verwendete Bibliotheken

**I2C** <br>
Master: wire.h für Datenübertragung <br>
Slave:  wire.h für Datenübertragung

**SPI** <br>
Master: ESP32DMASPIMaster.h (https://registry.platformio.org/libraries/hideakitai/ESP32DMASPI) für Datenübertragung, <br>
 cstring für Speicherbefehle <br>

Slave:  ESP32DMASPISlave.h  (https://registry.platformio.org/libraries/hideakitai/ESP32DMASPI) für Datenübertragung, <br>
 cstring für Speicherbefehle

## Software

Arduino IDE 2.2.1

## Funktionsweise

**I2C**

Der Master initialisiert das I2C-Interface und erstellt ein Bildarray (Graustufen), welches im weiteren Verlauf kontinuirlich an den Slave übertragen wird. Zusätzlich teilt der Master das Bild für die Übertragung in kleinere Pakete auf (Chunks). Jedes Paket enthält zusätzlich einen Header, bestehend aus einer Paketgröße und Sequenznummer. Dieser Header ermöglicht es dem Slave, die Daten (Pixelwerte) des Arrays durch eine Logik in der richtige Reihenfolge zu empfangen und zu verarbeiten. 

Nach der Initialisierung des I2C-Interfaces erstellt der Slave ein Bildarray (Sollbild) zur späteren Fehlerberechnung (prozentuale Abweichung vom Mittelwert der Pixeldaten). Hierbei wird das Sollbild im späteren Verlauf mit den empfangenen Ist-Bildern verglichen. Durch die implementierte Logik kann der Slave die Bilder kontinuirlich empfangen und verarbeiten. Nach dem Empfang von 100 Bildern errechnet der Slave die durchschnittliche Bildrate, Übertragungsgeschwindigkeit, Durchschnittliche Latenz, Latenzzeiten zwischen 100 empfangenen Bildern und die prozentuale Abweichung vom Mittelwert der Pixeldaten. Die Ausgabe der Berechneten Werte erfolgt über den Serial Monitor.

**SPI** 

Die Übertragung über SPI funktioniert ähnlich wie bei I2C. Der Master initialisiert das SPI-Interface und erstellt ein Bildarray (Graustufen), welches im weiteren Verlauf kontinuirlich an den Slave übertragen wird. Auch hierbei wird das Bild in einzelne Pakete (Chunks) aufgeteilt. Jedes Paket enthält wie bei der I2C-Übertragung einen Header, welcher aus einer Paketgröße und Sequenznummer besteht. Die Header ermöglichen es dem Slave, die Daten (Pixelwerte) des Arrays durch eine Logik in der richtigen Reihenfolge zu empfangen und zu verarbeiten.

Nach der Initialisierung des SPI-Interfaces erstellt der Slave ein Bildarray (Sollbild) zur späteren Fehlerberechnung (prozentuale Abweichung vom Mittelwert der Pixeldaten). Hierbei wird das Sollbild im späteren Verlauf mit den empfangenen Ist-Bildern verglichen. Durch die implementierte Logik kann der Slave die Bilder kontinuirlich empfangen und verarbeiten. Nach dem Empfang von 100 Bildern errechnet der Slave die durchschnittliche Bildrate, Übertragungsgeschwindigkeit, Durchschnittliche Latenz, Latenzzeiten zwischen 100 empfangenen Bildern und die prozentuale Abweichung vom Mittelwert der Pixeldaten. Die Ausgabe der Berechneten Werte erfolgt über den Serial Monitor.


## Berechnungen und Ausgaben

Nach dem empfang von 100 Bildern werden durch den Slave in der I2C-Konfiguration und SPI-Konfiguration folgende Parameter berechnet:

- Durchschnittliche Bildrate / FPS in Bilder pro Sekunde
- Durchschnittliche Übertragungsgeschwindigkeit in Kilobytes pro Sekunde
- Durchschnittliche prozentuale Abweichung der Mittelwerte (Pixel) in Prozent
- Durchschnittliche Latenz in Millisekunden
- Einzelne Latenzzeiten zwischen den empfangenen Bildern in Millisekunden
- Optional: Ausgabe der empfangenen Pixeldaten



## Setup und Verwendung

1. Verbindung / Verkabelung der Hardwarekomponenten:

**I2C**

Master:<br>
GPIO-PIN 18 = SCK (Serial Clock) <br>
GPIO-PIN 23 = SDA (Serial Data) <br>
GND               (Ground) <br>                   

Slave:<br>
GPIO-PIN 21 = SDA (Serial Data) <br>
GPIO-PIN 22 = SCK (Serial Clock) <br>
GND               (Ground) <br>

**SPI**

Master:<br>
GPIO-PIN 14 = SCK   (Serial Clock) <br>
GPIO-PIN 2  = MISO  (Master Input Slave Output) <br>
GPIO-PIN 13 = MOSI  (Master Output Slave Input) <br>
GPIO-PIN 15 = SS    (Slave Select) <br>
GND                 (Ground) <br>

Slave:<br>
GPIO-PIN 14 = SCK   (Serial Clock) <br>
GPIO-PIN 5  = MISO  (Master Input Slave Output) <br>
GPIO-PIN 13 = MOSI  (Master Output Slave Input) <br>
GPIO-PIN 15 = SS    (Slave Select) <br> 
GND                 (Ground) <br>


2. Code anpassen: Testszenario, Bildauflösung und Frequenz individuell anpassbar.

2. Hochladen der Codes

3. Auslesen der Messergebnisse nach 100 empfangenen Bildern im Serial Monitor.

## Testszenarien

**In den Codes lassen sich zwei Testszenarien definieren:** <br>

- Szenario 1: Bildübertragung von Bildern mit gleichen Pixelwerten: <br>
Alle Pixel haben den Wert "255"

oder

- Szenario 2: Bildübertragung von Bildern mit Pixelwerten in wiederkehrender Reihenfolge: <br>
Beginnend mit dem Wert "0", steigend bis zum Wert "255" und dann erneut beginnend bei 0 usw., bis zum Endwert, welcher sich durch die individuelle Auflösung ergibt <br>
0 ... 255 1 2 3 ... 255 1 2 ... usw.

## Limitationen und Probleme

**I2C**

- Größere Bildauflösungen als 220x200 Pixel können dafür sorgen, dass der DRAM nicht mehr ausreicht um den Code auf das Microcontrollersystem hochzuladen.
- Ab einer Frequenz von 235000 Hz kommt es zu Bildfehlern in Testszenario 2.
- Ab einer Frequenz von 236000 Hz findet keine Bildübertragung statt.

**SPI**

- Eine Anpassung der Frequenz hat keinen Einfluss auf die Performance
- Größere Bildauflösungen als 316x316 Pixel können dafür sorgen, dass dass der DRAM nicht mehr ausreicht um den Code auf das Microcontrollersystem hochzuladen.

## Lizenz

TH Köln


