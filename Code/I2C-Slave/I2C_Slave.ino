// Zweck: Benchmark I2C-Bildübertragung Slave (ESP32-DevKitC V4)
// Projekt: Bildübertragung auf ESP32
// Referenz: 113_IP_espcluster_VV
// Bibliotheken: wire.h
// Letzte Änderung: 25.05.2024 Ricardo Schickel

// Fügt die I2C-Bibliothek hinzu
#include "Wire.h"

// Pinbelegung der GPIO-Pins
const int SDA_PIN_MASTER = 21;
const int SCL_PIN_MASTER = 22;

// Definition der Slave-Adresse, Bildauflösung und Transfergröße
#define I2C_DEV_ADDR 0x55       // Adresse des Slaves
#define IMG_SIZE 10000          // Bildgröße, darf max. 51402 Bytes groß sein, sonst DRAM voll
#define MAX_PAYLOAD_SIZE 124    // Maximale Transfergröße

// Erstellung eines Arrays für das Sollbild und Istbild
uint8_t img[IMG_SIZE];           // Array für das empfangene Bild
uint8_t sollBild[IMG_SIZE];      // Array für das Sollbild

// Variablen zur Verfolgung des Bildempfangs und für Berechnungen
int imgIndex = 0;                // Aktueller Index im Bildarray
unsigned long totalData = 0;     // Gesamtzahl der übertragenen Daten in Bytes
unsigned long startTime = 0;     // Startzeit für die Messung
int receivedImages = 0;          // Anzahl der empfangenen Bilder
float totalDeviationSum = 0;     // Summe der Abweichungen

// Array zur Speicherung der Latenzzeiten jedes einzelnen empfangenen Bildes
uint32_t latenzZeiten[100];
uint32_t letztesBildZeit = 0;     // Zeitstempel des letzten empfangenen Bildes

// Initialisierung der I2C-Übertragung und Befüllung des Sollbilds mit Pixeldaten
void setup() {
  Serial.begin(921600);
  Wire.begin((uint8_t)I2C_DEV_ADDR);
  Wire.onReceive(onReceive);      // Funktion für den Empfang von Daten
  generateSollBild();             // Sollbild generieren
  letztesBildZeit = millis();     // Initialisierung des Zeitstempels für das erste Bild
  startTime = millis();           // Zeitstempel zur Definition der Startzeit
}

// Kontinuierlicher Bildempfang und Berechnung der durchschnittlichen Messwerte
void loop() {
  // Berechnungen nach 100 Bildern:
  if (receivedImages >= 100) { 
    float sumLatency = 0;
    for (int i = 0; i < receivedImages; i++) {
      sumLatency += latenzZeiten[i];
    }
    //Berechnung der durchschnittlichen Latenz zwischen 100 Bildern
    float durchschnittLatency = sumLatency / receivedImages;  

    // Berechnung der FPS basierend auf der durchschnittlichen Latenz (100 Bilder / Zeit)
    float durchschnittFps = receivedImages / (sumLatency / 1000.0);

    // Berechnung der Übertragungsgeschwindigkeit
    unsigned long endTime = millis();
    float gesamtzeit = (endTime - startTime) / 1000.0;
    float durchschnittGeschwindigkeit = totalData / gesamtzeit / 1024;

    // Berechnung der durchschnittlichen prozentualen Abweichung
    float durchschnittDeviation = totalDeviationSum / receivedImages;

    // Ausgabe der Berechnungen im Serial Monitor
    Serial.print("Durchschnittliche FPS: ");
    Serial.println(durchschnittFps, 2);
    Serial.print("Durchschnittliche Übertragungsgeschwindigkeit: ");
    Serial.print(durchschnittGeschwindigkeit, 2);
    Serial.println(" KB/s");
    Serial.print("Durchschnittliche prozentuale Abweichung: ");
    Serial.print(durchschnittDeviation, 2);
    Serial.println("%");
    Serial.print("Durchschnittliche Latenz: ");
    Serial.print(durchschnittLatency);
    Serial.println(" ms");
    printLatenzZeiten();

    // Zurücksetzen sämtlicher Zähler und Werte für die nächsten Berechnungen
    receivedImages = 0;
    totalData = 0;
    totalDeviationSum = 0;
    startTime = millis();
  }
}

// Funktion, die kontinuierlich aktiv ist, solange Daten über I2C empfangen werden
// Wird bereits im Setup ausgeführt
void onReceive(int howMany) {
  // Wenn mehr als 4 Bytes eines Pakets empfangen wurden: Definition und Befüllung des Header-Arrays
  if (howMany >= 4) { 
    uint8_t header[4];
    for (int i = 0; i < 4; i++) {
      header[i] = Wire.read();
    }

    // Prüfung ob Header ein Reset-Paket ist (wenn alle 4 Bytes des Header "0xFF" sind)
    if (header[0] == 0xFF && header[1] == 0xFF && header[2] == 0xFF && header[3] == 0xFF) {
      imgIndex = 0; // Zurücksetzen des Index für das nächste Bild
      return; // Frühzeitige Rückkehr, da es sich um ein Reset-Paket handelt
    } else {  // Wenn Header kein Reset-Paket ist, wird die Größe (Wert) der Chunksize festgelegt
      int chunkSize = header[0] | (header[1] << 8); // Definition der Chunksizevariable mit Größeninfos des Datenblocks
      if (imgIndex + chunkSize <= IMG_SIZE) {       // prüft ob Daten in ein Bildarray passen (Istbild)
        for(int i = 0; i < chunkSize; i++) {        // füllt die Daten ins Bildarray (Istbild)
          img[imgIndex++] = Wire.read();
        }
        totalData += chunkSize + 4;                 // Berechnung der Gesamtzahl der empfangenen Daten in Bytes (4 Bytes sind der Header)
      }
    }
  }

  // Wenn ein komplettes Bild empfangen wurde
  if (imgIndex >= IMG_SIZE) { 
    imgIndex = 0;                                   // Zurücksetzen des Index
    plotImage();                                  // Optional: Anzeigen der Pixelwerte, benötigt aber viel Leistung!

    // Berechnet den Mittelwert und Abweichung der Pixelwerte des IST-Bildes
    float meanIst = calculateMean(img, IMG_SIZE); 
    float deviation = calculateDeviation(calculateMean(sollBild, IMG_SIZE), meanIst);
    totalDeviationSum += deviation;

    receivedImages++;                                 // Erhöht den Zähler für empfangenes Bild um +1

    // Zeitmessung für das empfangene Bild
    unsigned long aktuelleZeit = millis();
    uint32_t latenz = aktuelleZeit - letztesBildZeit;
    letztesBildZeit = aktuelleZeit;
    if (receivedImages <= 100) {
      latenzZeiten[receivedImages - 1] = latenz;      // Speichern der Latenzzeiten zwischen jedem empfangenem Bild im Array
    }
  }
}

// Funktion zur Befüllung der Pixeldaten (Array Sollbild)
// Sollbildgenerierung für Szenario 1: Alle Pixelwerte haben den Wert 255
//void generateSollBild() {
//  for (int i = 0; i < IMG_SIZE; i++) {
//    sollBild[i] = 255; 
//  }
//}

// Sollbildgenerierung für Szenario 2: Wiederkehrende Reihenfolge der Pixelwerte 0 ... 255
 void generateSollBild() {
   for (int i = 0; i < IMG_SIZE; i++) {
     sollBild[i] = i % 256; // Werte von 0 bis 255 wiederholen
   }
 }

// Funktion zur Berechnung des Mittelwertes eines Arrays
float calculateMean(uint8_t* array, int size) {
  long sum = 0;
  for (int i = 0; i < size; i++) {
    sum += array[i];
  }
  return (float)sum / size;
}

// Funktion zur Berechnung der prozentualen Abweichung (Istbild vom Sollbild)
float calculateDeviation(float meanSoll, float meanIst) {
  return abs(meanIst - meanSoll) / meanSoll * 100.0;
}

// Funktion zur Anzeige der Pixelwerte im Serial Monitor
void plotImage() {
  Serial.print("Bild Pixelwerte: ");
  for (uint32_t i = 0; i < IMG_SIZE; i++) {
    Serial.print(img[i]);
    if (i < IMG_SIZE - 1) {
      Serial.print(" ");
    } else {
      Serial.println();
    }
  }
}

// Funktion zur Ausgabe der Latenzzeiten im Serial Monitor
void printLatenzZeiten() {
  Serial.println("Latenzzeiten (ms) für die letzten 100 Bilder:");
  for (uint16_t i = 0; i < 100; i++) {
    Serial.print(latenzZeiten[i]);
    if (i < 99) {
      Serial.print(", ");
    } else {
      Serial.println();
    }
  }
}
