// Zweck: Benchmark I2C-Bildübertragung Master (Waveshare ESP32 One)
// Projekt: Bildübertragung auf ESP32
// Referenz: 113_IP_espcluster_VV
// Bibliotheken: wire.h
// Letzte Änderung: 25.05.2024 Ricardo Schickel


// Fügt die I2C-Bibliothek hinzu
#include "Wire.h"

// Definition der GPIO-Pins 
const int SDA_PIN_MASTER = 18;
const int SCL_PIN_MASTER = 23;

// Definition der Slave-Adresse, Bildauflösung, Transfergröße und des Bildarrays f. den Versand
#define I2C_DEV_ADDR 0x55 // Adresse des Slaves
#define IMG_SIZE 10000 // Darf max. 51402 Bytes groß sein, sonst DRAM voll
#define MAX_PAYLOAD_SIZE 124 // 
uint8_t img[IMG_SIZE]; // Array für das Bild

// Initialisierung der I2C-Verbindung und Befüllung des Arrays für den Bildversand
void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true); // Aktiviert Debug-Ausgaben im SM
  Wire.begin(SDA_PIN_MASTER, SCL_PIN_MASTER); // Initialisierung I2C Kommunikation
  Wire.setClock(100000);

// Befüllung des Arrays mit Pixelwerten:
// Bildgenerierung für Szenario 1: Alle Pixelwerte haben den Wert 255
    //for (int i = 0; i < IMG_SIZE; i++) {
   //     img[i] = 255;
    //}


  // Bildgenerierung für Szenario 2: Wiederkehrende Reihenfolge der Pixelwerte 0 ... 255 1 2 ... 255
  for(int i = 0; i < IMG_SIZE; i++) {
    img[i] = i % 256;                                         // Werte von 0 bis 255 wiederholen
  }
}

// Funktion um Slave mitzuteilen, dass Übertragung zurückgesetzt werden soll (Reset-Paket)
// => Master sagt an Slave, dass die aktuelle Übertragung beendet ist
void sendResetPacket() {
    Wire.beginTransmission(I2C_DEV_ADDR);                     // Beginn der Übertragung zum Slave
    // Nutzt spezielle Werte für die Reset-Logik, sendet 4x die 255 als Hex-Wert-Byte
    Wire.write(0xFF);                                         // Sendet 255 als Hex-Wert
    Wire.write(0xFF);                                         // Sendet 255 als Hex-Wert
    Wire.write(0xFF);                                         // Sendet 255 als Hex-Wert
    Wire.write(0xFF);                                         // Sendet 255 als Hex-Wert
    Wire.endTransmission(true);                               // Kommunikation wird gestoppt
}

// Kontinuirlicher Bildversand in Schleife über Chunks (Pakete)
void loop() {
  static uint16_t sequence = 0;                               // Sequenznummer, startet bei 0
   

//Berechnung der Größe des aktuellen Pakets / Paketgröße
  for (int i = 0; i < IMG_SIZE; i += MAX_PAYLOAD_SIZE) {
    int chunkSize = min(MAX_PAYLOAD_SIZE, IMG_SIZE - i);      // Berechnung der Chunkgröße
    
    Wire.beginTransmission(I2C_DEV_ADDR);                     // Beginn der Übertragung zum Slave
    
    // Header senden: Paketgröße und Sequenznummer
    // Header helfen dem Slave zu wissen, wie groß der Chunk (Paket) ist und wie die Reihenfolge lautet
    Wire.write(chunkSize & 0xFF);                             // Sendet niedrigstes Byte von Chunksize (LSB)
    Wire.write((chunkSize >> 8) & 0xFF);                      // Sendet höchstes Byte von Chunksize (MSB)
    Wire.write(sequence & 0xFF);                              // Sendet niedrigstes Byte der Sequenznummer (LSB)
    Wire.write((sequence >> 8) & 0xFF);                       // Sendet höchstes Byte der Sequenznummer (MSB)
    
    // Senden der Bilddaten als aktuelles Paket               // Schleifendurchlauf durch jedes Byte im Chunk (Paket)
    for (int j = 0; j < chunkSize; j++) {                     // Senden des Byte i (Startindex: Bytes die bisher gesendet wurden)
      Wire.write(img[i + j]);                                 // Senden des Byte j (Position: Bytes die gesendet werden) 
    }
    Wire.endTransmission(true);                               // Beendet die Übertragung
    sequence++;                                               // Sequenznummer erhöhen
 
  }

  sendResetPacket();                                          // Sendet Reset-Paket am Ende jedes Bildtransfers
  sequence = 0;                                               // Sequenznummer für das nächste Bild zurücksetzen
  delay(1); 
}