// Zweck: Benchmark SPI-Bildübertragung Slave (ESP32-DevKitC V4)
// Projekt: Bildübertragung auf ESP32
// Referenz: 113_IP_espcluster_VV
// Bibliotheken: ESP32DMAISlave.h, cstring
// Letzte Änderung: 25.05.2024 Ricardo Schickel

// Hinzufügen der Bibliotheken
#include <ESP32DMASPISlave.h>
#include <cstring> // benötigt für Speicheroperationen (memcpy)

// Initialisiert das SPI-Slave-Objekt
ESP32DMASPI::Slave slave;

// Pinbelegung der GPIO-Pins für SPI
const uint8_t PIN_SCK = 14;
const uint8_t PIN_MISO = 5; 
const uint8_t PIN_MOSI = 13;
const uint8_t PIN_SS = 15;

// Definition der Bildauflösung und Transfergröße
const uint16_t IMAGE_WIDTH = 100; 
const uint16_t IMAGE_HEIGHT = 100;
const uint32_t IMAGE_SIZE = IMAGE_WIDTH * IMAGE_HEIGHT; // Gesamtauflösung des Bildes
const uint16_t MAX_TRANSFER_SIZE = 4000; // Maximale Übertragungsgröße (max. 4092 Bytes)
const uint8_t HEADER_SIZE = 4;

// Definition der Zeiger auf den DMA-Speicherbereich und auf den Speicherbereich des Sollbilds
uint8_t* dma_rx_buffer;
uint8_t* image_buffer;
uint8_t sollBild[IMAGE_SIZE]; // Erstellung eines Arrays für das Sollbild zur Berechnung der Abweichung

float gesamtdaten = 0;          // Übertragene Gesamtdaten (Anfangswert)
float gesamteDeviationSum = 0;  // Summe der Abweichungen

// Array zur Speicherung der Latenzzeiten
uint32_t latenzZeiten[100];
uint32_t letztesBildZeit = 0; // Zeitstempel des letzten empfangenen Bildes

// Initialisierung der SPI-Übertragung und Befüllung des Sollbilds mit Pixelwerten
void setup() {
    Serial.begin(921600); 
    
    // Zuweisung des DMA-Buffers (Speichers)  
    dma_rx_buffer = slave.allocDMABuffer(MAX_TRANSFER_SIZE + HEADER_SIZE);
    image_buffer = new uint8_t[IMAGE_SIZE];

    slave.begin(VSPI, PIN_SCK, PIN_MISO, PIN_MOSI, PIN_SS);
    slave.setMaxTransferSize(MAX_TRANSFER_SIZE + HEADER_SIZE);

    generateSollBild();
    letztesBildZeit = millis(); // Initialisierung des Zeitstempels für das erste Bild
}

// Schleife für den kontinuierlichen Bildempfang
void loop() {
    static size_t totalReceived = 0; // Zähler für die Gesamtanzahl der empfangenen Bytes
    static uint16_t expectedSequence = 0; // Sequenznummer die der Slave erwartet
    static uint32_t startTime = millis(); // Startzeit für die Zeitmessung
    static int bilderEmpfangen = 0; // Bildzähler für empfangene Bilder

    // Empfang der Daten in den Speicherbereich (dma_rx_buffer) 
    // und Entnahme von Chunkgröße & Sequenznummer aus den Headern:
    size_t received = slave.transfer(nullptr, dma_rx_buffer, MAX_TRANSFER_SIZE + HEADER_SIZE); // received = Variable für empfangene Bytes
    if (received > HEADER_SIZE) { // Wenn empfangene Daten größer als 4 Byte:
        uint16_t chunkSize = dma_rx_buffer[0] | (dma_rx_buffer[1] << 8); // entnimmt Chunkgröße aus ersten beiden Bytes des Buffers (Chunksize ist nur Variable mit dem Wert der Größe)
        uint16_t sequence = dma_rx_buffer[2] | (dma_rx_buffer[3] << 8); // entnimmt Sequenznummer aus dem dritten und vierten Bytes des Buffers

        // Kopieren der Daten in den Bildpuffer und Zusammensetzung des Bildes
        // Wenn Sequenznummer der erwarteten Sequenznummer entspricht und die Chunk-Größe <= Transfergröße dann...
        if (sequence == expectedSequence && chunkSize <= MAX_TRANSFER_SIZE) {
            memcpy(&image_buffer[totalReceived], dma_rx_buffer + HEADER_SIZE, chunkSize); // kopiere die empfangenen Daten in ein Array (Istbild) 
            totalReceived += chunkSize; // Erhöhung des Zählers um die Größe des aktuellen Chunks
            gesamtdaten += chunkSize + HEADER_SIZE; // Aktualisierung der Gesamtdatenmenge und des Headers
            expectedSequence++; // Erhöhung der Sequenznummer für nächsten Datenchunk
        }

        // Überprüfung ob ein Bild empfangen wurde
        // Wenn Gesamtanzahl der empfangenen Bytes größer ist als die Bildgröße dann:
        if (totalReceived >= IMAGE_SIZE) {
            totalReceived = 0; // Setzt Zähler für Gesamtzahl der empfangenen Bytes zurück
            expectedSequence = 0; // Setzt die erwartete Sequenznummer zurück
            bilderEmpfangen++; // erhöht den Bildzähler wenn ein Bild empfangen
            //plotImage(); // Optional: empfangene Bilder als Pixeldaten anzeigen lassen

            // Zeitmessung für das empfangene Bild
            uint32_t aktuelleZeit = millis();
            uint32_t latenz = aktuelleZeit - letztesBildZeit;
            letztesBildZeit = aktuelleZeit;
            if (bilderEmpfangen <= 100) {
                latenzZeiten[bilderEmpfangen - 1] = latenz; // Speichern der Latenzzeit im Array
            }

            // Berechnung des durchschnittlichen Mittelwerts des Bildes und Abweichung vom Sollbild
            float meanIst = calculateMean(image_buffer, IMAGE_SIZE);
            float deviation = calculateDeviation(calculateMean(sollBild, IMAGE_SIZE), meanIst);
            gesamteDeviationSum += deviation; // Aktualisierung der Summe der Abweichungen

            // Ausgabe der Berechnungen nach 100 Bildern:
            if (bilderEmpfangen >= 100) {
                float sumLatency = 0;
                for (int i = 0; i < bilderEmpfangen; i++) {
                    sumLatency += latenzZeiten[i];
                }
                float durchschnittLatency = bilderEmpfangen / (sumLatency / 1000.0);

                // Berechnung der FPS basierend auf der durchschnittlichen Latenz (100Bilder/Zeit)
                float durchschnittFps = bilderEmpfangen / (sumLatency / 1000.0);

                // Berechnung der Übertragungsgeschwindigkeit
                float gesamtzeit = (aktuelleZeit - startTime) / 1000.0;
                float durchschnittGeschwindigkeit = gesamtdaten / gesamtzeit / 1024;

                // Berechnung der durchschnittlichen prozentualen Abweichung
                float durchschnittDeviation = gesamteDeviationSum / bilderEmpfangen;

                // Ausgabe der Berechnungen im Serial Monitor:
                Serial.print("Durchschnittliche FPS: ");
                Serial.println(durchschnittFps, 2);
                Serial.print("Durchschnittliche Übertragungsgeschwindigkeit: ");
                Serial.print(durchschnittGeschwindigkeit, 2);
                Serial.println(" KB/s");
                Serial.print("Durchschnittliche prozentuale Abweichung: ");
                Serial.print(durchschnittDeviation, 2);
                Serial.println("%");
                Serial.print("Durchschnittliche Latenz: ");
                Serial.print(durchschnittLatency);
                Serial.println(" ms");
                printLatenzZeiten();

                // Zurücksetzen sämtlicher Zähler und Werte
                bilderEmpfangen = 0;
                gesamtdaten = 0;
                gesamteDeviationSum = 0;
                startTime = millis();
            }
        }
    }
}

// Sollbildgenerierung für Szenario 1: Alle Pixelwerte haben den Wert 255
//void generateSollBild() {
//    for (uint32_t i = 0; i < IMAGE_SIZE; i++) {
//        sollBild[i] = 255;
//    }
//}


// Sollbildgenerierung für Szenario 2: Alle Pixelwerte in wiederkehrender Reihenfolge 0 ... 255 usw.
void generateSollBild() {
    for (uint32_t i = 0; i < IMAGE_SIZE; i++) {
        sollBild[i] = i % 256;
    }
}

// berechnet den Mittelwert des Arrays
float calculateMean(const uint8_t* array, uint32_t size) {
    uint32_t sum = 0;
    for (uint32_t i = 0; i < size; i++) {
        sum += array[i];
    }
    return (float)sum / size;
}

// Berechnung der prozentualen Abweichung des Istwertes vom Sollwert
float calculateDeviation(float meanSoll, float meanIst) {
    return abs(meanIst - meanSoll) / meanSoll * 100.0;
}

// Funktion zur Anzeige der Pixelwerte im Serial Monitor
void plotImage() {
    Serial.print("Bild Pixelwerte: ");
    for (uint32_t i = 0; i < IMAGE_SIZE; i++) {
        Serial.print(image_buffer[i]);
        if (i < IMAGE_SIZE - 1) {
            Serial.print(" ");
        } else {
            Serial.println();
        }
    }
}

// Funktion zur Ausgabe der Latenzzeiten im Serial Monitor
void printLatenzZeiten() {
    Serial.println("Latenzzeiten (ms) für die letzten 100 Bilder:");
    for (uint16_t i = 0; i < 100; i++) {
        Serial.print(latenzZeiten[i]);
        if (i < 99) {
            Serial.print(", ");
        } else {
            Serial.println();
        }
    }
}
