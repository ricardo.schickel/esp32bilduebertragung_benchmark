// Zweck: Benchmark SPI-Bildübertragung Master (Waveshare ESP32 One)
// Projekt: Bildübertragung auf ESP32
// Referenz: 113_IP_espcluster_VV
// Bibliotheken: ESP32DMASPIMaster.h
// Letzte Änderung: 26.05.2024 Ricardo Schickel


#include <ESP32DMASPIMaster.h>

// Initialisiert das SPI-Master-Objekt
ESP32DMASPI::Master master;

// Pinbelegung der GPIO-Pins
const int PIN_SCK = 14; // Clock-Pin
const int PIN_MISO = 2; // 2 für ESP32 one, alternativ 5 für Espressif v.4
const int PIN_MOSI = 13; // Master Out Slave In Pin
const int PIN_SS = 15; // Slave Select Pin

// Definition der Bildauflösung, Transfergröße und Headergröße
const size_t IMAGE_WIDTH = 100; // Bildbreite
const size_t IMAGE_HEIGHT = 100; // Bildhöhe
const size_t BUFFER_SIZE = IMAGE_WIDTH * IMAGE_HEIGHT; // Gesamtauflösung des Bildes als Puffergröße
const size_t MAX_TRANSFER_SIZE = 4000; // Maximale Übertragungsgröße (max. 4092 Bytes)
const size_t HEADER_SIZE = 4; // Größe des Headers im Datenpaket

// Zeiger auf den dynamisch zugewiesenen DMA-Speicherbereich
uint8_t* dmaBuffer;

// Array zur Speicherung der Bilddaten
uint8_t imageBuffer[BUFFER_SIZE];

void setup() {
    Serial.begin(115200); // Initialisiert die serielle Kommunikation mit 115200 Baud

    // Allokiert den DMA-Speicher für das Bild und den Header
    dmaBuffer = master.allocDMABuffer(BUFFER_SIZE + HEADER_SIZE);

    // Initialisiert die Bilddaten im imageBuffer (Szenario 1: Alle Pixelwerte haben den Wert 255)
    //for (size_t i = 0; i < BUFFER_SIZE; i++) {
    //    imageBuffer[i] = 255;
    //}


    // Initialisiert die Bilddaten im imageBuffer (Szenario 2: Alle Pixelwerte in wiederkehrender Reihenfolge 0 ... 255)
    for (size_t i = 0; i < BUFFER_SIZE; i++) {
        imageBuffer[i] = i % 256;
    }

    // Initialisiert den SPI-Master
    master.begin(VSPI, PIN_SCK, PIN_MISO, PIN_MOSI, PIN_SS);
    master.setDataMode(SPI_MODE0); // Setzt den SPI-Datenmodus auf Mode 0
    master.setFrequency(1000000); // Setzt die SPI-Frequenz auf 1 MHz, Erhöhung bringt aber keine Performance
    master.setMaxTransferSize(MAX_TRANSFER_SIZE + HEADER_SIZE); // Setzt die maximale Transfergröße

    pinMode(PIN_SS, OUTPUT); // Setzt den SS-Pin als Ausgang
    digitalWrite(PIN_SS, HIGH); // Setzt den SS-Pin auf HIGH (deaktiviert)
}

// Kontinuierlicher Bildversand in Schleife
void loop() {
    static uint16_t sequence = 0;

    // Bildversand in mehreren Teilen
    for (size_t offset = 0; offset < BUFFER_SIZE; offset += MAX_TRANSFER_SIZE) {
        size_t chunkSize = min(MAX_TRANSFER_SIZE, BUFFER_SIZE - offset); // Berechnet die Größe des aktuellen Chunks

        // Setzt die Header-Informationen für den aktuellen Chunk
        dmaBuffer[0] = chunkSize & 0xFF;                // Speichert das niedrigste Byte der Chunkgröße im ersten Byte des DMA-Buffers (LSB)
        dmaBuffer[1] = (chunkSize >> 8) & 0xFF;         // Speichert das höchste Byte der Chunkgröße im zweiten Byte des DMA-Buffers (MSB)
        dmaBuffer[2] = sequence & 0xFF;                 // Speichert das niedrigste Byte der Sequenznummer im dritten Byte des DMA-Buffers (LSB)
        dmaBuffer[3] = (sequence >> 8) & 0xFF;          // Speichert das höchste Byte der Sequenznummer im vierten Byte des DMA-Buffers (MSB)

        // Kopiert die Pixelwerte des Testbildes aus dem imageBuffer in den dmaBuffer
        memcpy(dmaBuffer + HEADER_SIZE, imageBuffer + offset, chunkSize);

        // Zeigt falls benötigt übertragene Pakete im Serial Monitor an
        Serial.print("Übertrage Chunk: ");
        Serial.print(offset / MAX_TRANSFER_SIZE);
        Serial.print(" mit Größe: ");
        Serial.println(chunkSize);

        // Aktiviert den Slave und versendet die Daten
        digitalWrite(PIN_SS, LOW);
        master.transfer(dmaBuffer, nullptr, chunkSize + HEADER_SIZE); // Überträgt die Daten
        digitalWrite(PIN_SS, HIGH);

        sequence++; // Erhöht die Sequenznummer
        delay(1); // Minimale Verzögerung für maximale Effizienz
    }

    sequence = 0; // Setzt die Sequenznummer zurück
}
